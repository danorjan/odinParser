package token

type TokenType := string;

ILLEGAL: string :"ILLEGAL";
EOF: string :"EOF";

IDENT: string :"IDENT";
INT: string :"INT";

ASSIGN: string :"=";
PLUS: string :"+";

COMMA: string :",";
SEMICOLON: string :";";

LPAREN: string :"(";
RPAREN: string :")";
LBRACE: string :"{";
RBRACE: string :"}";

FUNCTION: string :"FUNCTION";
LET: string :"LET";


Token :: struct {
    type: TokenType,
    Literal: string,
}
